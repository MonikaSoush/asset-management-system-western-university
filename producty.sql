-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 10, 2022 at 08:37 AM
-- Server version: 5.7.30
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `productify`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subcategory_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `assigned` int(11) NOT NULL DEFAULT '0',
  `unit_id` int(11) DEFAULT NULL,
  `is_damage` varchar(3) DEFAULT 'no',
  `purchase_date` date DEFAULT NULL,
  `depreciation` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `category_id`, `name`, `price`, `updated_at`, `created_at`, `subcategory_id`, `code`, `status`, `staff_id`, `assigned`, `unit_id`, `is_damage`, `purchase_date`, `depreciation`) VALUES
(1, 1, 'DeskforReacher', '50.0000', '2022-12-03 03:45:18', '2022-11-29 09:51:36', 1, 'C001-Sub001-1', 0, NULL, 0, 100, 'yes', '2022-11-29', NULL),
(2, 1, 'Red Marker', '2.0000', '2022-12-03 03:45:11', '2022-11-29 09:52:14', 3, 'C001-Sub003-2', 1, NULL, 1, 10, 'no', '2022-11-29', NULL),
(3, 3, 'Luxis570', '10000.0000', '2022-12-03 03:44:59', '2022-11-30 08:36:09', 4, 'C003-Sub004-3', 1, NULL, 1, 1, 'no', '2022-01-02', NULL),
(4, 3, 'Luxis550', '10000.0000', '2022-12-03 03:44:39', '2022-12-03 03:33:48', 4, 'C003-Sub004-4', 1, NULL, 0, 2, 'no', '2022-12-01', NULL),
(5, 2, 'Whiteboard', '100.0000', '2022-12-03 03:44:06', '2022-12-03 03:36:27', 1, 'C002-Sub001-5', 1, NULL, 0, 2, 'no', '2022-12-01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assignstaffs`
--

CREATE TABLE `assignstaffs` (
  `id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assignstaffs`
--

INSERT INTO `assignstaffs` (`id`, `floor_id`, `room_id`, `asset_id`, `date`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 2, '2022-11-29 00:00:00', '2022-11-29 16:52:33', '2022-11-29 16:52:33'),
(2, 4, 2, 3, '2022-11-30 00:00:00', '2022-11-30 15:36:57', '2022-11-30 15:36:57');

-- --------------------------------------------------------

--
-- Table structure for table `branchs`
--

CREATE TABLE `branchs` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branchs`
--

INSERT INTO `branchs` (`id`, `code`, `name`, `location`, `status`, `created_at`, `updated_at`) VALUES
(1, '01', 'TK', 'Toul Kork', 1, '2022-09-19 08:50:45', '2022-09-19 08:50:45'),
(2, '02', 'TSP', 'Toul Svay Prey', 1, '2022-09-19 08:57:27', '2022-09-19 09:04:46'),
(3, '03', 'KPC', 'Kampong Cham Province', 1, '2022-09-19 08:57:39', '2022-09-19 08:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `code`, `name`, `slug`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'C001', 'Office', 'office', 'Office', 1, '2022-11-29 09:43:04', '2022-11-29 09:43:04'),
(2, 'C002', 'Library', 'library', 'Library', 1, '2022-11-29 09:43:29', '2022-11-29 09:43:29'),
(3, 'C003', 'Accesotry', 'accesotry', 'Accesotry', 1, '2022-11-30 08:35:10', '2022-11-30 08:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `depreciates`
--

CREATE TABLE `depreciates` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `asset_id` int(11) NOT NULL,
  `depreciation_bym` decimal(25,4) DEFAULT NULL,
  `depreciation` int(11) DEFAULT NULL,
  `depreciation_enddate` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `depreciates`
--

INSERT INTO `depreciates` (`id`, `created_at`, `updated_at`, `asset_id`, `depreciation_bym`, `depreciation`, `depreciation_enddate`, `status`) VALUES
(6, '2022-12-03 03:31:21', '2022-12-03 03:31:21', 2, '0.1667', 12, '2023-11-29', 1),
(7, '2022-12-03 03:46:04', '2022-12-03 03:46:04', 3, '416.6667', 24, '2024-01-02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exp_cat_id` bigint(20) UNSIGNED NOT NULL,
  `expense_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(10,2) NOT NULL,
  `expense_date` date NOT NULL,
  `expense_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

CREATE TABLE `expense_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `finished_products`
--

CREATE TABLE `finished_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `porcessing_pro_id` bigint(20) UNSIGNED NOT NULL,
  `sub_cat_id` bigint(20) UNSIGNED NOT NULL,
  `finished_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rejected_quantities` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantities` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished_date` date NOT NULL,
  `finished_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE `floors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `name`, `details`, `created_at`, `updated_at`) VALUES
(1, 'TK', 'TK', '2022-11-20 04:49:38', '2022-11-19 23:18:22'),
(4, 'Floor1', 'Floor1', '2022-11-19 23:22:14', '2022-11-19 23:22:14'),
(5, 'Floor2', 'Floor2', '2022-11-19 23:22:26', '2022-11-19 23:22:26'),
(6, 'Fllor 3', 'Floor3', '2022-11-22 06:47:51', '2022-11-22 06:47:51');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `key`, `display_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'company_name', 'Company Name', 'Asset Management', '2022-09-19 07:53:26', '2022-09-19 07:55:59'),
(2, 'compnay_tagline', 'Compnay Tagline', 'Asset Management', '2022-09-19 07:53:26', '2022-09-19 07:55:59'),
(3, 'email_address', 'Email Address', 'rynaboy22@gmail.com', '2022-09-19 07:53:26', '2022-09-21 08:32:17'),
(4, 'phone_number', 'Phone Number', '0987654321', '2022-09-19 07:53:26', '2022-09-19 07:55:59'),
(5, 'address', 'Address', 'PP', '2022-09-19 07:53:26', '2022-09-19 07:55:59'),
(6, 'currency_name', 'Currency Name', 'USD', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(7, 'currency_symbol', 'Currency Symbol', '$', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(8, 'currency_position', 'Currency Position', 'left', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(9, 'timezone', 'Timezone', 'Asia/Phnom_Penh', '2022-09-19 07:53:26', '2022-09-19 07:55:59'),
(10, 'purchase_code_prefix', 'Purchase Code Prefix', 'PUR-', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(11, 'processing_code_prefix', 'Processing Code Prefix', 'PRO-', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(12, 'finished_code_prefix', 'Finished Code Prefix', 'FIN-', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(13, 'transferred_code_prefix', 'Transferred Code Prefix', 'TRA-', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(14, 'starting_purchase_code', 'Starting Purchase Code', '1', '2022-09-19 07:53:26', '2022-09-19 07:53:26'),
(15, 'logo', 'Logo', '5406436851669048171.jpg', '2022-09-19 07:53:26', '2022-11-21 09:29:31'),
(16, 'small_logo', 'Small Logo', '17376441671663774323.jpg', '2022-09-19 07:53:26', '2022-09-21 08:32:03'),
(17, 'dark_logo', 'Dark Logo', '7060829761669048171.jpg', '2022-09-19 07:53:26', '2022-11-21 09:29:31'),
(18, 'small_dark_logo', 'Small Dark Logo', '5947320261669048171.jpg', '2022-09-19 07:53:26', '2022-11-21 09:29:31'),
(19, 'favicon', 'Favicon', '4956478351663774323.jpg', '2022-09-19 07:53:26', '2022-09-21 08:32:03'),
(20, 'copyright', 'Copyright', 'Copyright © 2022 Ryna All rights reserved', '2022-09-19 07:53:26', '2022-09-19 07:55:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_04_12_000000_create_users_table', 1),
(2, '2020_04_12_000001_create_password_resets_table', 1),
(3, '2020_04_12_000002_create_failed_jobs_table', 1),
(4, '2020_04_12_000003_create_general_settings_table', 1),
(5, '2020_04_12_000004_create_units_table', 1),
(6, '2020_04_12_000005_create_sizes_table', 1),
(7, '2020_04_12_000006_create_payment_methods_table', 1),
(8, '2020_04_12_000007_create_processing_steps_table', 1),
(9, '2020_04_12_000008_create_showrooms_table', 1),
(10, '2020_04_12_000009_create_categories_table', 1),
(11, '2020_04_12_000010_create_sub_categories_table', 1),
(12, '2020_04_12_000011_create_suppliers_table', 1),
(13, '2020_04_12_000012_create_staff_table', 1),
(14, '2020_04_12_000013_create_purchases_table', 1),
(15, '2020_04_12_000014_create_purchase_products_table', 1),
(16, '2020_04_12_000015_create_processing_products_table', 1),
(17, '2020_04_12_000017_create_finished_products_table', 1),
(18, '2020_04_12_000018_create_transferred_products_table', 1),
(19, '2020_04_20_200639_create_processing_product_staff_pivot_table', 1),
(20, '2020_10_05_123157_create_expense_categories_table', 1),
(21, '2020_10_05_125157_create_expenses_table', 1),
(22, '2020_10_07_115920_create_purchase_returns_table', 1),
(23, '2020_10_09_145941_create_purchase_damages_table', 1),
(24, '2020_10_11_171511_create_used_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `slug`, `code`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cash', 'cash', 'cash', '', 1, '2022-09-19 08:11:32', '2022-09-19 08:11:32'),
(2, 'ABA', 'aba', 'aba', '', 1, '2022-09-19 08:11:39', '2022-09-19 08:11:39');

-- --------------------------------------------------------

--
-- Table structure for table `processing_products`
--

CREATE TABLE `processing_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `processing_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `processing_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `processing_product_staff`
--

CREATE TABLE `processing_product_staff` (
  `processing_product_id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` bigint(20) UNSIGNED DEFAULT NULL,
  `processing_step_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `processing_steps`
--

CREATE TABLE `processing_steps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_date` date NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` double(10,2) DEFAULT NULL,
  `discount` double(10,2) DEFAULT NULL,
  `trasnport` double(10,2) DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  `total_paid` double(10,2) DEFAULT NULL,
  `total_due` double(10,2) DEFAULT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `purchase_date`, `supplier_id`, `purchase_code`, `sub_total`, `discount`, `trasnport`, `total`, `total_paid`, `total_due`, `payment_type`, `purchase_image`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, '2022-11-19', 1, '1', 1.00, 0.00, NULL, 1.00, 1.00, 0.00, NULL, '', '', 1, '2022-11-19 01:50:17', '2022-11-19 01:50:17'),
(2, '2022-11-19', 1, '2', 1.00, 0.00, NULL, 1.00, 1.00, 0.00, 'aba', '', '', 1, '2022-11-19 01:51:41', '2022-11-19 01:51:41'),
(3, '2022-11-19', 1, '3', 3.00, 0.00, NULL, 3.00, 2.00, 1.00, NULL, '', '', 1, '2022-11-19 01:52:48', '2022-11-20 20:35:49'),
(4, '2022-11-21', 1, '4', 1.00, 0.00, NULL, 1.00, 1.00, 0.00, NULL, '', '', 1, '2022-11-21 01:39:47', '2022-11-21 01:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_damages`
--

CREATE TABLE `purchase_damages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `damage_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `damage_date` date NOT NULL,
  `damage_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_products`
--

CREATE TABLE `purchase_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `quantity` double(10,2) NOT NULL,
  `used_quantity` double(10,2) DEFAULT NULL,
  `return_quantity` double(10,2) DEFAULT NULL,
  `damage_quantity` double(10,2) DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_price` double(10,2) NOT NULL,
  `discount` double(10,2) DEFAULT NULL,
  `total` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_products`
--

INSERT INTO `purchase_products` (`id`, `purchase_id`, `product_name`, `room_id`, `floor_id`, `quantity`, `used_quantity`, `return_quantity`, `damage_quantity`, `unit`, `unit_price`, `discount`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-19 01:50:17', '2022-11-19 01:50:17'),
(2, 2, '12344', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-19 01:51:41', '2022-11-19 01:51:41'),
(3, 3, '23333', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-19 01:52:48', '2022-11-19 01:52:48'),
(4, 3, '333333', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-19 01:52:48', '2022-11-19 01:52:48'),
(5, 3, '0000', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-20 20:35:49', '2022-11-20 20:35:49'),
(6, 4, 'test', NULL, NULL, 1.00, NULL, NULL, NULL, 'pcs', 1.00, NULL, 1.00, '2022-11-21 01:39:47', '2022-11-21 01:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_returns`
--

CREATE TABLE `purchase_returns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `return_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `return_date` date NOT NULL,
  `refund_amount` double(10,2) DEFAULT NULL,
  `return_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reference_no`
--

CREATE TABLE `reference_no` (
  `id` int(1) NOT NULL,
  `item_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reference_no`
--

INSERT INTO `reference_no` (`id`, `item_code`) VALUES
(1, '6');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `floor_id`, `created_at`, `updated_at`) VALUES
(1, '203', 1, '2022-11-19 22:36:32', '2022-11-20 20:30:03'),
(2, '204', 1, '2022-11-19 23:03:52', '2022-11-19 23:03:52'),
(3, '2', 4, '2022-11-19 23:17:23', '2022-11-20 20:34:57'),
(4, '205', 6, '2022-11-22 06:48:16', '2022-11-22 06:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `showrooms`
--

CREATE TABLE `showrooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `email`, `phone_number`, `department`, `designation`, `address`, `profile_picture`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ryna', 'rynaboy22@gmail.com', '0969894789', 'IT', 'Information Technology', 'PP', '', 1, '2022-09-19 09:58:51', '2022-09-19 09:58:51'),
(2, 'Dara', 'info@dara.vb', '0987654321', 'IT', 'Information Technology', 'PP', '', 1, '2022-09-20 09:37:50', '2022-09-20 09:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `sizes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `code`, `name`, `slug`, `category_id`, `sizes`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sub001', 'Table', 'table', 1, '1', 'Table', 1, '2022-11-29 09:44:33', '2022-11-29 09:44:33'),
(2, 'Sub002', 'Whiteboard', 'whiteboard', 1, '1', 'Whiteboard', 1, '2022-11-29 09:46:08', '2022-11-29 09:46:08'),
(3, 'Sub003', 'Markers', 'markers', 1, '1', 'Markers', 1, '2022-11-29 09:46:50', '2022-11-29 09:46:50'),
(4, 'Sub004', 'Car', 'car', 3, '1', 'Car', 1, '2022-11-30 08:35:35', '2022-11-30 08:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `email`, `phone_number`, `company_name`, `designation`, `address`, `profile_picture`, `status`, `created_at`, `updated_at`) VALUES
(1, 'TSD Store', 'info@gg.com', '0987654321', 'TSD Store', NULL, '', '', 1, '2022-09-19 08:07:32', '2022-09-19 08:07:32');

-- --------------------------------------------------------

--
-- Table structure for table `transferred_products`
--

CREATE TABLE `transferred_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `finished_id` bigint(20) UNSIGNED NOT NULL,
  `showroom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `transferred_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transferred_date` date NOT NULL,
  `cartoon_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transferred_quantities` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transferred_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `slug`, `code`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Pcs', 'pcs', 'pcs', '', 1, '2022-09-19 08:12:11', '2022-09-19 08:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `used_products`
--

CREATE TABLE `used_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `finished_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_pro_id` bigint(20) UNSIGNED NOT NULL,
  `used_quantity` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `profile_picture`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'System Admin', 'rynaboy22@gmail.com', NULL, '$2y$10$EJTA/.nT8kB55Rmk5BUUfeY/cjlMiqFA9K33sXMsnUtAuV0Sb98u.', NULL, '1', 1, NULL, '2022-09-19 07:53:20', '2022-09-19 07:54:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignstaffs`
--
ALTER TABLE `assignstaffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branchs`
--
ALTER TABLE `branchs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depreciates`
--
ALTER TABLE `depreciates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_exp_cat_id_index` (`exp_cat_id`);

--
-- Indexes for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `finished_products`
--
ALTER TABLE `finished_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `finished_products_porcessing_pro_id_index` (`porcessing_pro_id`),
  ADD KEY `finished_products_sub_cat_id_index` (`sub_cat_id`);

--
-- Indexes for table `floors`
--
ALTER TABLE `floors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processing_products`
--
ALTER TABLE `processing_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `processing_products_purchase_id_index` (`purchase_id`);

--
-- Indexes for table `processing_product_staff`
--
ALTER TABLE `processing_product_staff`
  ADD KEY `processing_product_staff_processing_product_id_index` (`processing_product_id`),
  ADD KEY `processing_product_staff_staff_id_index` (`staff_id`),
  ADD KEY `processing_product_staff_processing_step_id_index` (`processing_step_id`);

--
-- Indexes for table `processing_steps`
--
ALTER TABLE `processing_steps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchases_supplier_id_index` (`supplier_id`);

--
-- Indexes for table `purchase_damages`
--
ALTER TABLE `purchase_damages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_damages_purchase_id_index` (`purchase_id`);

--
-- Indexes for table `purchase_products`
--
ALTER TABLE `purchase_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_products_purchase_id_index` (`purchase_id`);

--
-- Indexes for table `purchase_returns`
--
ALTER TABLE `purchase_returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_returns_purchase_id_index` (`purchase_id`);

--
-- Indexes for table `reference_no`
--
ALTER TABLE `reference_no`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `showrooms`
--
ALTER TABLE `showrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_index` (`category_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transferred_products`
--
ALTER TABLE `transferred_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transferred_products_finished_id_index` (`finished_id`),
  ADD KEY `transferred_products_showroom_id_index` (`showroom_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `used_products`
--
ALTER TABLE `used_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `used_products_finished_id_index` (`finished_id`),
  ADD KEY `used_products_purchase_pro_id_index` (`purchase_pro_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `assignstaffs`
--
ALTER TABLE `assignstaffs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `branchs`
--
ALTER TABLE `branchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `depreciates`
--
ALTER TABLE `depreciates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense_categories`
--
ALTER TABLE `expense_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `finished_products`
--
ALTER TABLE `finished_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `floors`
--
ALTER TABLE `floors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `processing_products`
--
ALTER TABLE `processing_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `processing_steps`
--
ALTER TABLE `processing_steps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchase_damages`
--
ALTER TABLE `purchase_damages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_products`
--
ALTER TABLE `purchase_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchase_returns`
--
ALTER TABLE `purchase_returns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reference_no`
--
ALTER TABLE `reference_no`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `showrooms`
--
ALTER TABLE `showrooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transferred_products`
--
ALTER TABLE `transferred_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `used_products`
--
ALTER TABLE `used_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_exp_cat_id_foreign` FOREIGN KEY (`exp_cat_id`) REFERENCES `expense_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `finished_products`
--
ALTER TABLE `finished_products`
  ADD CONSTRAINT `finished_products_porcessing_pro_id_foreign` FOREIGN KEY (`porcessing_pro_id`) REFERENCES `processing_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `finished_products_sub_cat_id_foreign` FOREIGN KEY (`sub_cat_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `processing_products`
--
ALTER TABLE `processing_products`
  ADD CONSTRAINT `processing_products_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `processing_product_staff`
--
ALTER TABLE `processing_product_staff`
  ADD CONSTRAINT `processing_product_staff_processing_product_id_foreign` FOREIGN KEY (`processing_product_id`) REFERENCES `processing_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `processing_product_staff_processing_step_id_foreign` FOREIGN KEY (`processing_step_id`) REFERENCES `processing_steps` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `processing_product_staff_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_damages`
--
ALTER TABLE `purchase_damages`
  ADD CONSTRAINT `purchase_damages_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_products`
--
ALTER TABLE `purchase_products`
  ADD CONSTRAINT `purchase_products_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_returns`
--
ALTER TABLE `purchase_returns`
  ADD CONSTRAINT `purchase_returns_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transferred_products`
--
ALTER TABLE `transferred_products`
  ADD CONSTRAINT `transferred_products_finished_id_foreign` FOREIGN KEY (`finished_id`) REFERENCES `finished_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transferred_products_showroom_id_foreign` FOREIGN KEY (`showroom_id`) REFERENCES `showrooms` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `used_products`
--
ALTER TABLE `used_products`
  ADD CONSTRAINT `used_products_finished_id_foreign` FOREIGN KEY (`finished_id`) REFERENCES `finished_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `used_products_purchase_pro_id_foreign` FOREIGN KEY (`purchase_pro_id`) REFERENCES `purchase_products` (`id`) ON DELETE CASCADE;
