@extends('layouts.pdf')

@section('content-area')
    <h3>@lang('All Assetx')</h3>
    <table class="table-listing table table-bordered">
        <thead>
            <tr>
                <th>@lang('#')</th>
                <th>{{ __('Items Code') }}</th>
                <th>{{ __('Asset Name') }}</th>
                <th>{{ __('Unit') }}</th>
                <th>{{ __('Book Value') }}</th>
                <th>{{ __('Purchase Date') }}</th>
                <th>{{ __('Categories Note') }}</th>
                <th>{{ __('Sub-Categories Note') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Assign') }}</th>
                <th>{{ __('Depreciation') }}</th>
                <th>{{ __('Created') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($assets as $key => $asset)
               <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    <a href="{{ route('assets.edit', $asset->id) }}">
                                        {{ $asset->code }}</a>
                                </td>
                                <td>{{ $asset->name }} </td>
                                <td>{{ $asset->unit_id}} </td>
                                <td>{{ number_format($asset->price,2) }} </td>
                                <td>{{ \Carbon\Carbon::parse($asset->purchase_date)->format('d-M-Y') }} </td>
                                <td>{{ $asset->categoryname }} </td>
                                <td>{{ $asset->scname }} </td>
                                <td>
                                    @if ($asset->isActive())
                                        <span class="badge badge-success">{{ __('Active') }}</span>
                                    @else
                                        <span class="badge badge-warning">{{ __('Damage') }}</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($asset->isAssign())
                                        <span class="badge badge-success">{{ __('Assigned') }}</span>
                                    @else
                                        <span class="badge badge-warning">{{ __('Not Assign') }}</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($asset->depreciation)
                                        {{ $asset->depreciation.'m' }}
                                    @else

                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($asset->created_at)->format('d-M-Y') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
