@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header mb-4">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ __('Depreciation') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ __('Depreciation') }}</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="col-md-12">
                @include('admin.includes.alert')
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-5 col-6 mb-2">
                    <form action="{{ route('depreciates.index') }}" method="GET" role="search">
                        <div class="input-group">
                            <input type="text" name="term" placeholder="{{ __('Type name or code...') }}"
                                    class="form-control" autocomplete="off"
                                    value="{{ request('term') ? request('term') : '' }}" required>
                            <span class="input-group-append">
                                    <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                                </span>
                        </div>
                    </form>
                </div>
                <div class="col-lg-9 col-md-7 col-6">
                    <div class="card-tools text-md-right">
                        <a class="btn btn-secondary" href="{{ route('categories.pdf') }}">
                            <i class="fas fa-download"></i> @lang('Export')
                        </a>
                        <a href="{{ route('depreciates.create') }}" class="btn btn-primary">
                            {{ __('Add Depreciates') }} <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="p-0 table-responsive table-custom my-3">
                <table class="table">
                    <thead>
                    <tr>
                        <th>@lang('#')</th>
                        <th>{{ __('Item Code') }}</th>
                        <th>{{ __('Asset Name') }}</th>
                        <th>{{ __('Unit') }}</th>
                        <th>{{ __('Book Value') }}</th>
                        <th>{{ __('Purchase Date') }}</th>
                        <th>{{ __('Depreciation Period') }}</th>
                        <th>{{ __('Depreciation ByMonth') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Depreciation end Date') }}</th>
                        <th>{{ __('Created') }}</th>
                        <th class="text-right">{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if ($depreciates->total() > 0)
                        @foreach ($depreciates as $key => $depreciate)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    <a href="">
                                       {{ $depreciate->assetcode }}</a>
                                </td>
                                <td>{{ $depreciate->assetname }} </td>
                                <td>{{ $depreciate->unit }} </td>
                                <td>{{ $depreciate->price }} </td>
                                <td>{{ $depreciate->purchase_date }} </td>
                                <td>{{ $depreciate->depreciation }} </td>
                                <td>{{ $depreciate->depreciation_bym }} </td>
                                <td>
                                    @if ($depreciate->isActive())
                                        <span class="badge badge-success">{{ __('Active') }}</span>
                                    @else
                                        <span class="badge badge-warning">{{ __('Damage') }}</span>
                                    @endif
                                </td>
                                <td>{{ $depreciate->depreciation_enddate }} </td>
                                <td>{{ \Carbon\Carbon::parse($depreciate->created_at)->format('d-M-Y') }}</td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-secondary dropdown-toggle action-dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            @if ($depreciate->isActive())
                                                <a href="{{ route('depreciates.status', $depreciate->id) }}"
                                                    class="dropdown-item"><i class="fas fa-window-close"></i>
                                                    {{ __('Inactive') }}</a>
                                            @else
                                                <a href="{{ route('depreciates.status', $depreciate->id) }}"
                                                    class="dropdown-item"><i class="fas fa-check-square"></i>
                                                    {{ __('Active') }}</a>
                                            @endif
                                            <a href="{{ route('depreciates.delete', $depreciate->id) }}"
                                                class="dropdown-item delete-btn"
                                                data-msg="{{ __('Are you sure you want to delete this Depreciate?') }}"><i
                                                    class="fas fa-trash"></i> {{ __('Delete') }}</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">
                                <div class="data_empty">
                                    <img src="{{ asset('img/result-not-found.svg') }}" alt="" title="">
                                    <p>{{ __('Sorry, no Depreciate found in the database. Create your very first Depreciate.') }}</p>
                                    <a href="{{ route('depreciates.create') }}" class="btn btn-primary">
                                        {{ __('Add Depreciate') }} <i class="fas fa-plus-circle"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot class="footer_table">
                        @if ($depreciates->total() > 0)
                        <tr>
                            <th>@lang('#')</th>
                            <th>{{ __('Item Code') }}</th>
                            <th>{{ __('Asset Name') }}</th>
                            <th>{{ __('Unit') }}</th>
                            <th>{{ __('Book Value') }}</th>
                            <th>{{ __('Purchase Date') }}</th>
                            <th>{{ __('Depreciation Period') }}</th>
                            <th>{{ __('Depreciation ByMonth') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Depreciation end Date') }}</th>
                            <th>{{ __('Created') }}</th>
                            <th class="text-right">{{ __('Action') }}</th>
                        </tr>
                        @endif
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->


            <!-- pagination start -->
            {{ $depreciates->links() }}
            <!-- pagination end -->
        </div>
    </div>
@endsection
