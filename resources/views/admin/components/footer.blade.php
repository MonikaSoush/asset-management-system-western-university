<footer class="main-footer">
    <strong>
        {{ $settings['copyright'] ?? '' }}
    </strong>
</footer>
