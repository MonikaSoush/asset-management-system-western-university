@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header mb-4">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ __('Edit Asset Move') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('assignstaffs.index') }}">{{ __('Asset Move') }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ __('Edit Asset Move') }}</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __('Edit Asset') }}: {{ $asset->name }}</h3>
                <div class="card-tools">
                    <a href="{{ route('assets.index') }}" class="btn btn-block btn-primary">
                        <i class="fas fa-long-arrow-alt-left"></i> {{ __('Go Back') }}
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <form class="form-horizontal" action="{{ route('assets.update', $asset->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="categoryName">{{ __('Category Name') }}<span class="required-field">*</span></label>
                                <select class="advance-select-box form-control @error('categoryName') is-invalid @enderror" id="categoryName" name="categoryName"  required>
                                    <option value="" selected disabled>{{ __('Select a category') }}</option>
                                    @foreach($categories as $key => $category)
                                        <option value="{{ $category->id }}" {{ $category->id == $asset->category_id ? 'selected' : ''  }} >{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="subcategoryName">{{ __('SubCategory Name') }}<span class="required-field">*</span></label>
                                <select class="advance-select-box form-control @error('subcategoryName') is-invalid @enderror" id="subcategoryName" name="subcategoryName"  required>
                                    <option value="" selected disabled>{{ __('Select a subcategoryName') }}</option>
                                    @foreach($subcategories as $key => $subcategory)
                                        <option value="{{ $subcategory->id }}" {{ $subcategory->id == $asset->subcategory_id ? 'selected' : ''  }} >{{ $subcategory->name }}</option>
                                    @endforeach
                                </select>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="code">{{ __('Asset Code') }}<span class="required-field">*</span></label>
                                <input type="text" class="form-control @error('code') is-invalid @enderror" id="code" name="code" placeholder="{{ __('Asset Code') }}" value="{{ $asset->code }}" required>
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">{{ __('Asset Name') }}<span class="required-field">*</span></label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="{{ __('Asset Name') }}" value="{{ $asset->name }}" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="price">{{ __('Price') }}</label>
                                <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" placeholder="{{ __('Price') }}" value="{{ $asset->price }}">
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="status" class="col-form-label">{{ __('Unit') }}</label>
                                <input type="number" class="form-control @error('unit') is-invalid @enderror" id="unit" name="unit" placeholder="{{ __('Value') }}" value="{{ $asset->unit_id }}">
                                @error('unit')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="datep" class="col-form-label">{{ __('Purchase Date') }}</label>
                                <input type="date" name="purchase_date" class="form-control" value="{{ $asset->purchase_date }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="status" class="col-form-label">{{ __('Status') }}</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ $asset->isActive() ? "selected" : ""}}>{{ __('Active') }}</option>
                                    <option value="0" {{ $asset->isActive() ? "" : "selected"}}>{{ __('Damage') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> {{ __('Save Changes') }}</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.content -->
@endsection


