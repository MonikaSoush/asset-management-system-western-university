@extends('layouts.admin')

@section('extra-style')
<link href="{{ asset('css/select2/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/select2/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header mb-4">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ __('Assign Asset') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('assignstaffs.index') }}">{{ __('Assign Asset') }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ __('Assign Asset') }}</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __('Assign Asset') }}</h3>
                <div class="card-tools">
                    <a href="{{ route('assignstaffs.index') }}" class="btn btn-block btn-primary">
                        <i class="fas fa-long-arrow-alt-left"></i> {{ __('Go Back') }}
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <form class="form-horizontal" action="{{ route('assignstaffs.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="Date">{{ __('Assign Date') }}<span class="required-field">*</span></label>
                                <input type="date" placeholder="{{\Carbon\Carbon::today()->format('Y-m-d')}}" class="@error('Date') is-invalid @enderror form-control" id="Date" name="date" required>
                                @error('Date')
                                <span class="invalid-feedback date-invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="assetName">{{ __('Asset Name') }}<span class="required-field">*</span></label>
                                <select class="advance-select-box form-control @error('assetName') is-invalid @enderror" id="assetName" name="assetName"  required>
                                    <option value="" selected disabled>{{ __('Select a Asset') }}</option>
                                    @foreach($assets as $key => $asset)
                                        <option value="{{ $asset->id }}">{{ $asset->name }}</option>
                                    @endforeach
                                </select>
                                @error('assetName')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="assetName">{{ __('Floor') }}<span class="required-field">*</span></label>
                                <select class="advance-select-box form-control @error('floor') is-invalid @enderror" id="floor_id" name="floor_id"  required>
                                    <option value="" selected disabled>{{ __('Select Floor') }}</option>
                                    @foreach($floors as $key => $floor)
                                        <option value="{{ $floor->id }}">{{ $floor->name }}</option>
                                    @endforeach
                                </select>
                                @error('floor')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="assetName">{{ __('Room') }}<span class="required-field">*</span></label>
                                <select class="advance-select-box form-control @error('room') is-invalid @enderror" id="room_id" name="room_id"  required>
                                    <option value="" selected disabled>{{ __('Select Room') }}</option>
                                    @foreach($rooms as $key => $room)
                                        <option value="{{ $room->id }}">{{ $room->name }}</option>
                                    @endforeach
                                </select>
                                @error('room')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{ __('Save') }}</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.content -->
@endsection

@section('extra-script')
<script src="{{ asset('js/select2/select2.min.js') }}"></script>
@endsection

