@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header mb-4">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ __('Assets') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ __('Assets') }}</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="col-md-12">
                @include('admin.includes.alert')
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-5 col-6 mb-2">
                    <form action="{{ route('assets.index') }}" method="GET" role="search">
                        <div class="input-group">
                            <input type="text" name="term" placeholder="{{ __('Type name or code...') }}"
                                    class="form-control" autocomplete="off"
                                    value="{{ request('term') ? request('term') : '' }}" required>
                            <span class="input-group-append">
                                    <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                                </span>
                        </div>
                    </form>
                </div>
                <div class="col-lg-9 col-md-7 col-6">
                    <div class="card-tools text-md-right">
                        <a class="btn btn-secondary" href="{{ route('assets.pdf') }}">
                            <i class="fas fa-download"></i> @lang('Export PDF')
                        </a>
                        <a class="btn btn-secondary" href="{{ route('assets.export') }}">
                            <i class="fa fa-file-excel"></i> @lang('Export Excel')
                        </a>
                        <a href="{{ route('assets.addimport') }}" class="btn btn-primary">
                            {{ __('Import Asset') }} <i class="fas fa-plus-circle"></i>
                        </a>
                        <a href="{{ route('assets.create') }}" class="btn btn-primary">
                            {{ __('Add Asset') }} <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="p-0 table-responsive table-custom my-3">
                <table class="table">
                    <thead>
                    <tr>
                        <th>@lang('#')</th>
                        <th>{{ __('Items Code') }}</th>
                        <th>{{ __('Asset Name') }}</th>
                        <th>{{ __('Unit') }}</th>
                        <th>{{ __('Book Value') }}</th>
                        <th>{{ __('Purchase Date') }}</th>
                        <th>{{ __('Categories Note') }}</th>
                        <th>{{ __('Sub-Categories Note') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Assign') }}</th>
                        <th>{{ __('Depreciation') }}</th>
                        <th>{{ __('Created') }}</th>
                        <th class="text-right">{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if ($assets->total() > 0)
                        @foreach ($assets as $key => $asset)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    <a href="{{ route('assets.edit', $asset->id) }}">
                                        {{ $asset->code }}</a>
                                </td>
                                <td>{{ $asset->name }} </td>
                                <td>{{ $asset->unit_id}} </td>
                                <td>{{ number_format($asset->price,2) }} </td>
                                <td>{{ \Carbon\Carbon::parse($asset->purchase_date)->format('d-M-Y') }} </td>
                                <td>{{ $asset->categoryname }} </td>
                                <td>{{ $asset->scname }} </td>
                                <td>
                                    @if ($asset->isActive())
                                        <span class="badge badge-success">{{ __('Active') }}</span>
                                    @else
                                        <span class="badge badge-warning">{{ __('Damage') }}</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($asset->isAssign())
                                        <span class="badge badge-success">{{ __('Assigned') }}</span>
                                    @else
                                        <span class="badge badge-warning">{{ __('Not Assign') }}</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($asset->depreciation)
                                        {{ $asset->depreciation.'m' }}
                                    @else

                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($asset->created_at)->format('d-M-Y') }}</td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-secondary dropdown-toggle action-dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            @if ($asset->isActive())
                                                <a href="{{ route('assets.status', $asset->id) }}"
                                                    class="dropdown-item"><i class="fas fa-window-close"></i>
                                                    {{ __('Damage') }}</a>
                                            @else
                                                <a href="{{ route('assets.status', $asset->id) }}"
                                                    class="dropdown-item"><i class="fas fa-check-square"></i>
                                                    {{ __('Active') }}</a>
                                            @endif
                                            <a href="{{ route('assets.edit', $asset->id) }}"
                                                class="dropdown-item"><i class="fas fa-edit"></i>
                                                {{ __('Edit') }}</a>
                                            <a href="{{ route('assets.delete', $asset->id) }}"
                                                class="dropdown-item delete-btn"
                                                data-msg="{{ __('Are you sure you want to delete this asset?') }}"><i
                                                    class="fas fa-trash"></i> {{ __('Delete') }}</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">
                                <div class="data_empty">
                                    <img src="{{ asset('img/result-not-found.svg') }}" alt="" title="">
                                    <p>{{ __('Sorry, no asset found in the database. Create your very first asset.') }}</p>
                                    <a href="{{ route('assets.create') }}" class="btn btn-primary">
                                        {{ __('Add asset') }} <i class="fas fa-plus-circle"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot class="footer_table">
                        @if ($assets->total() > 0)
                        <tr>
                        <th>@lang('#')</th>
                        <th>{{ __('Items Code') }}</th>
                        <th>{{ __('Asset Name') }}</th>
                        <th>{{ __('Unit') }}</th>
                        <th>{{ __('Book Value') }}</th>
                        <th>{{ __('Purchase Date') }}</th>
                        <th>{{ __('Categories Note') }}</th>
                        <th>{{ __('Sub-Categories Note') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Assign') }}</th>
                        <th>{{ __('Depreciation') }}</th>
                        <th>{{ __('Created') }}</th>
                        <th class="text-right">{{ __('Action') }}</th>
                    </tr>
                    @endif
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->

            <!-- pagination start -->
            {{ $assets->links() }}
            <!-- pagination end -->
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            {{-- <form class="form-horizontal" action="{{ route('assets.asset_damage', $asset->id) }}" method="post" enctype="multipart/form-data"> --}}
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="reason" class="col-form-label">{{ __('Reason') }}</label>
                        <textarea class="form-control @error('reason') is-invalid @enderror" id="reason" name="reason" placeholder="{{ __('Reason') }}">{{ old('reason') }}</textarea>
                        @error('reason')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{ __('Save') }}</button>
                    </div>
                </div>
            </div>
            </form>
        </div>

        </div>
    </div>
</div>
@endsection
