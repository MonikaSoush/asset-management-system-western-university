@extends('layouts.admin')

@section('content')
    {{--   Content Header (Page header) --}}
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ __('Dashboard') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    {{--  end content-header --}}

    {{-- Main content --}}
    <div class="content">
        <div class="container-fluid">
            {{-- @if($stats->staff > 0 && $stats->suppliers > 0 && $stats->categories > 0 && $stats->subCats > 0 && $stats->purchases > 0) --}}
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3 stats">
                        <a href="{{ route('staff.index') }}">
                            <div class="info-box">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">{{ __('Staff') }}</span>
                                    <span class="info-box-number">{{ $stats->staff }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 stats">
                        <a href="{{ route('finished.index') }}">
                            <div class="info-box">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-boxes"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">{{ __('Asset') }}</span>
                                    <span class="info-box-number">{{ $stats->assets }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 stats">
                        <a href="{{ route('transferred.index') }}">
                            <div class="info-box">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-building"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">{{ __('Floor') }}</span>
                                    <span class="info-box-number">{{ $stats->floors }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 stats">
                        <a href="{{ route('transferred.index') }}">
                            <div class="info-box">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-window-maximize"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">{{ __('Room') }}</span>
                                    <span class="info-box-number">{{ $stats->rooms }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="card card-success w-100">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Category in last 12 months') }}</h3>
                        </div>
                        <div class="card-body">
                            {!! $categoryChart->container() !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="card card-success w-100">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Room Created in last 12 months') }}</h3>
                        </div>
                        <div class="card-body">
                            {!! $roomChart->container() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--  end main content --}}
@endsection

@section('extra-script')
    {{-- ChartScript --}}
    <script src="{{ asset('js/Chart.min.js') }}" charset=utf-8></script>
    @if($purchasesChart)
        {!! $purchasesChart->script() !!}
    @endif

    @if($finishedQtyChart)
        {!! $finishedQtyChart->script() !!}
    @endif

    @if($transferredQtyChart)
        {!! $transferredQtyChart->script() !!}
    @endif

    @if($expenseChart)
        {!! $expenseChart->script() !!}
    @endif

    @if($categoryChart)
        {!! $categoryChart->script() !!}
    @endif
    @if($roomChart)
        {!! $roomChart->script() !!}
    @endif
@endsection

