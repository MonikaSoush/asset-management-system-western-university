<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'name', 'price', 'status', 'category_id', 'subcategory_id','staff_id','unit_id','assigned','is_damage','purchase_date','depreciation'
    ];

    public function isActive()
    {
        return $this->status == 1 ? true : false;
    }

    public function isAssign()
    {
        return $this->assigned == 1 ? true : false;
    }

    public function isDamage()
    {
        return $this->damage == 'yes' ? true : false;
    }
}
