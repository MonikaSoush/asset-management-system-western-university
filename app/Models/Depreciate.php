<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Depreciate extends Model
{
    use HasFactory;
    protected $fillable = [
        'asset_id','depreciation_bym', 'depreciation', 'depreciation_enddate', 'status'
    ];
    public function isActive()
    {
        return $this->status == 1 ? true : false;
    }
}
