<?php

namespace App\Http\Controllers;

use App\Models\Room;
use DB;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $query = DB::table('rooms')
                ->join('floors', 'rooms.floor_id', '=', 'floors.id')
                ->orderBy('floors.order', 'asc')
                ->select('*','rooms.id as rid','rooms.created_at as rcdate','rooms.name as rname','floors.name as floor_name')
                ->paginate(15);
        if (request('term')) {
            $term = request('term');
            $query->where('name', 'Like', '%' . $term . '%');
        }

        $rooms = $query;
        return view('admin.rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $floors = DB::table('floors')->get();
        return view('admin.rooms.create', compact('floors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories',
        ]);

        // store category
        $rooms = Room::create([
            'name' => $request->name,
            'floor_id' => $request->floor_id
        ]);
        return redirect()->route('rooms.index')->withSuccess('Floor added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function show(Room $floor)
    {
        return redirect()->route('rooms.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floors = DB::table('floors')->get();
        $rooms = Room::where('id', $id)->first();
        return view('admin.rooms.edit', compact('rooms','floors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $rooms = Room::where('id', $id)->first();

        //validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories,name,' . $rooms->id,
        ]);

        // update category
        $rooms->update([
            'name' => $request->name,
            'floor_id' => $request->floor_id
        ]);
        return redirect()->route('rooms.index')->withSuccess('Room updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floors = Room::where('id', $id)->first();
        // destroy category
        $floors->delete();
        return redirect()->route('rooms.index')->withSuccess('Room deleted successfully!');
    }
}
