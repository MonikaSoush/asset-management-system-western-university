<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Depreciate;
use Illuminate\Http\Request;
use DB;

class DepreciateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Depreciate::query();
        $query =    $query->leftJoin('assets', 'depreciates.asset_id', '=', 'assets.id');
        $query = $query->select('depreciates.*','assets.name as assetname','assets.code as assetcode','assets.unit_id as unit','assets.price as price','assets.purchase_date as purchase_date');
        if (request('term')) {
            $term = request('term');
            $query->where('name', 'Like', '%' . $term . '%');
        }
        $depreciates = $query->orderBy('id', 'DESC')->paginate(15);
        return view('admin.depreciates.index', compact('depreciates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assets = Asset::where('status',1)->get();
        return view('admin.depreciates.create', compact('assets'));
    }

    public function changeStatus($slug)
    {
        $depre = Depreciate::where('id', $slug)->first();
        if ($depre->status == 1) {
            $depre->update([
                'status' => 0
            ]);
        } else {
            $depre->update([
                'status' => 1
            ]);
        }
        return redirect()->route('depreciates.index')->withSuccess('Depreciate status changed successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'asset' => 'required',
        ]);
        $asset_id =    $request->asset;
        $assets = Asset::where('id',$asset_id)->get();
        foreach ($assets as $key => $asset) {
            $price = $asset->price;
            $startd = strtotime($asset->purchase_date);
        }
        $depreciate = $request->depreciation;
        $startdate = strtotime(date("Y-m-d", $startd) . "+".$depreciate." months");
        $end_date = date('Y-m-d',$startdate);
        $debym = $price / $depreciate;

        $depreciates = Depreciate::create([
            'asset_id' => $request->asset,
            'depreciation_bym' => $debym,
            'depreciation' => $request->depreciation,
            'depreciation_enddate' => $end_date,
            'status' => $request->status
        ]);
        Asset::where('id', $request->asset)
       ->update([
           'depreciation' => $request->depreciation
        ]);
        return redirect()->route('depreciates.index')->withSuccess('Depreciates added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Depreciate  $depreciate
     * @return \Illuminate\Http\Response
     */
    public function show(Depreciate $depreciate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Depreciate  $depreciate
     * @return \Illuminate\Http\Response
     */
    public function edit(Depreciate $depreciate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Depreciate  $depreciate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Depreciate $depreciate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Depreciate  $depreciate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $depres = Depreciate::where('id', $id)->first();
        $depres->delete();
        return redirect()->route('depreciates.index')->withSuccess('Depreciates deleted successfully!');
    }
}
