<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PDF;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::query();
        if (request('term')) {
            $term = request('term');
            $query->where('name', 'Like', '%' . $term . '%')
                ->orWhere('email', 'Like', '%' . $term . '%');
        }
        $users = $query->orderBy('id', 'DESC')->paginate(15);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:80|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'profilePic' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        //upload selected image
        $imageName = '';
        if (isset($request->profilePic)) {
            $imagePath = 'img/profile';
            $imageName = $this->uploadImage($imagePath, $request->profilePic);
        }

        // store staff
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'profile_picture' => $imageName,
            'role' => $request->accountType,
            'status' => $request->status,
        ]);

        return redirect()->route('users.index')->withSuccess('User added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        // validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50',
            'password' => $request->password ? 'nullable|string|min:8|max:255|confirmed' : '',
            'profilePic' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        $imageName = $user->profile_picture;

        // remove current image and upload new image
        if (isset($request->profilePic)) {
            $this->deleteImage('img/profile/' . $user->profile_picture);
            $imagePath = 'img/profile';
            $imageName = $this->uploadImage($imagePath, $request->profilePic);
        }

        $request->password ? $password = Hash::make($request->password) : $password = $user->password;

        // update user
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'profile_picture' => $imageName,
            'role' => $request->accountType,
            'status' => $request->status,
        ]);

        return redirect()->route('users.index')->withSuccess('User updated successfully!');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        // delete user image from storage
        $this->deleteImage('img/profile/' . $user->profile_picture);

        // delete user
        $user->delete();
        return redirect()->route('users.index')->withSuccess('User deleted successfully!');
    }


    /**
     * Change the status of specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id)
    {
        $user = User::findOrFail($id);

        // change staff status
        if ($user->status == 1) {
            $user->update([
                'status' => 0
            ]);
        } else {
            $user->update([
                'status' => 1
            ]);
        }
        return redirect()->route('users.index')->withSuccess('User status changed successfully!');
    }

    // create pdf
    public function createPDF()
    {
        // retreive all records from db
        $data = User::latest()->get();
        // share data to view
        view()->share('users', $data);
        $pdf = PDF::loadView('admin.pdf.users', $data->all());
        // download PDF file with download method
        return $pdf->download('user-list.pdf');
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */

    public function checkUploadedFileProperties($extension, $fileSize)
    {
    $valid_extension = array("csv", "xlsx");
    $maxFileSize = 2097152;
    if (in_array(strtolower($extension), $valid_extension)) {
    if ($fileSize <= $maxFileSize) {
    } else {
    throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    } else {
    throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
    }
    }

    public function import()
    {
        $file = request()->file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $this->checkUploadedFileProperties($extension, $fileSize);
        $location = 'uploads';
        $file->move($location, $filename);
        $filepath = public_path($location . "/" . $filename);

        if($extension == "csv"){
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);
            if ($i == 0) {
            $i++;
            continue;
            }
            for ($c = 0; $c < $num; $c++) {
            $importData_arr[$i][] = $filedata[$c];

            }

            $i++;
            }
            fclose($file);

            $j = 0;
            foreach ($importData_arr as $importData) {
            $name = $importData[0];
            $email[] = $importData[1];
            $user = DB::table('users')->where('email',$email)->count();
            $j++;
                try {
                DB::beginTransaction();
                    User::create([
                        'name' => $importData[0],
                        'email' => $importData[1],
                        'password' => Hash::make($importData[2]),
                        'rule' => 1,
                        'status' => 1
                    ]);
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollBack();
                    }
            }
            return redirect()->route('users.addimport')->withSuccess('User Import Successful!');

            // $users = DB::table('users')->insert([
            //     'name'     => $importData_arrp[0],
            //     'email'    => $importData_arrp[1],
            //     'password' => Hash::make($row['password']),
            //     'rule' => 1,
            //     'status' => 1,
            // ]);

            }else{
            return redirect()->route('users.addimport')->withSuccess('Wrong File extention!');
        }






        // $file = request()->file('file')->getClientOriginalExtension();
        // $filename = request()->file('file');

        //     $file = fopen($filepath, "r");
        //     $importData_arr = array(); // Read through the file and store the contents as an array
        //     $i = 0;
        //     //Read the contents of the uploaded file
        //     while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
        //     $num = count($filedata);
        //     // Skip first row (Remove below comment if you want to skip the first row)
        //     if ($i == 0) {
        //     $i++;
        //     continue;
        //     }
        //     for ($c = 0; $c < $num; $c++) {
        //     $importData_arr[$i][] = $filedata[$c];
        //     }
        //     $i++;
        //     }
        //     fclose($file);

        //     // if(){

        //     // }else{
        //         // Excel::import(new UsersImport,$filename);
        //         //
        //     // }


    }

    public function addimport()
    {
        return view('admin.users.import');
    }
}
