<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use Illuminate\Http\Request;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Floor::query();
        if (request('term')) {
            $term = request('term');
            $query->where('name', 'Like', '%' . $term . '%');
        }

        $floors = $query->orderBy('order', 'asc')->paginate(15);
        return view('admin.floors.index', compact('floors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.floors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories',
            'details' => 'nullable|string|max:255',
        ]);

        // store category
        $category = Floor::create([
            'name' => $request->name,
            'order' => $request->order,
            'details' => clean($request->note)
        ]);
        return redirect()->route('floors.index')->withSuccess('Floor added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function show(Floor $floor)
    {
        return redirect()->route('floors.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floors = Floor::where('id', $id)->first();
        return view('admin.floors.edit', compact('floors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $floors = Floor::where('id', $id)->first();

        //validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories,name,' . $floors->id,
            'details' => 'nullable|string|max:255',
        ]);

        // update category
        $floors->update([
            'name' => $request->name,
            'order' => $request->order,
            'details' => clean($request->note),
        ]);
        return redirect()->route('floors.index')->withSuccess('Floor updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floors = Floor::where('id', $id)->first();
        // destroy category
        $floors->delete();
        return redirect()->route('floors.index')->withSuccess('Floor deleted successfully!');
    }
}
