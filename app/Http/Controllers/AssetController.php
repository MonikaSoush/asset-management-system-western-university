<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Staff;
use App\Models\Unit;
use Illuminate\Http\Request;
use PDF;
use DB;
use App\Exports\AssetsExport;
use App\Imports\AssetImport;
use Maatwebsite\Excel\Facades\Excel;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Asset::query();
        $query =    $query->leftJoin('sub_categories', 'sub_categories.id', '=', 'assets.subcategory_id')
                          ->leftJoin('categories', 'categories.id', '=', 'assets.category_id')
                          ->leftJoin('staff', 'staff.id', '=', 'assets.staff_id')
                          ->leftJoin('units', 'units.id', '=', 'assets.unit_id');
        $query = $query->select('assets.*','categories.name as categoryname','staff.name as staffname','units.name as unitname','sub_categories.name as scname');
        if (request('term')) {
            $term = request('term');
            $query->where('assets.name', 'Like', '%' . $term . '%');
        }
        $assets = $query->orderBy('id', 'DESC')->paginate(15);
        $staff = Staff::where('status', 1)->get();
        return view('admin.assets.index', compact('assets','staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 1)->get();
        $subcategories = SubCategory::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();
        return view('admin.assets.create', compact('categories','subcategories','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'nullable|string|max:255',
        ]);
        $icno = DB::table('reference_no')->where('id',1)->get();
        foreach ($icno as $key => $icnos) {
            $icno1 = $icnos->item_code;
        }
        $cat = Category::where('id',$request->categoryName)->get();
        $subcat = SubCategory::where('id',$request->subcategoryName)->get();
            foreach ($cat as $key => $cats) {
                $ccat = $cats->code;
            }
            foreach ($subcat as $key => $subcats) {
                $cscat = $subcats->code;
            }
        $code = $ccat.'-'.$cscat.'-'.$icno1;
        // store category
        $asset = Asset::create([
            'code' => $code,
            'name' => $request->name,
            'unit_id' => $request->unit,
            'category_id' => $request->categoryName,
            'subcategory_id' => $request->subcategoryName,
            'price' => $request->price,
            'depreciation' => $request->depreciation,
            'purchase_date' => $request->purchase_date,
            'status' => $request->status
        ]);
        $icno2 = $icno1 + 1;

        DB::update('update reference_no set item_code = '.$icno2.' where id = ?', ['1']);

        return redirect()->route('assets.index')->withSuccess('Asset added successfully!');
    }

    public function changeStatus($slug)
    {
        $asset = Asset::where('id', $slug)->first();

        // change category status
        if ($asset->status == 1) {
            $asset->update([
                'status' => 0
            ]);
        } else {
            $asset->update([
                'status' => 1
            ]);
        }
        return redirect()->route('assets.index')->withSuccess('Asset status changed successfully!');
    }

    public function asset_damage($id){
        $asset = Asset::where('id', $id)->first();
        $asset->update([
            'is_damage' => 'yes'
        ]);
        return redirect()->route('assets.index')->withSuccess('Asset Damges!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        return redirect()->route('assets.index');
    }

    public function addstaff()
    {
        return redirect()->route('assets.addstaff');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::where('id', $id)->first();
        $categories = Category::where('status', 1)->get();
        $subcategories = SubCategory::where('status', 1)->get();
        return view('admin.assets.edit', compact('asset','categories','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = Asset::where('id', $id)->first();

        //validate form
        $validator = $request->validate([
            'code' => 'required|string|max:50' . $asset->id,
            'name' => 'nullable|string|max:255',
        ]);

        // update branchs
        $asset->update([
            'code' => $request->code,
            'name' => $request->name,
            'unit_id' => $request->unit,
            'category_id' => $request->categoryName,
            'subcategory_id' => $request->subcategoryName,
            'price' => $request->price,
            'depreciation' => $request->depreciation,
            'purchase_date' => $request->purchase_date,
            'status' => $request->status
        ]);
        return redirect()->route('assets.index')->withSuccess('Asset updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assets = Asset::where('id', $id)->first();
        // destroy branch
        $assets->delete();
        return redirect()->route('assets.index')->withSuccess('Branch deleted successfully!');
    }

    public function createPDF()
    {
        // retreive all records from db
        $data = Asset::latest()->get();
        // share data to view
        view()->share('assets', $data);
        $pdf = PDF::loadView('admin.pdf.assets', $data->all());
        // download PDF file with download method
        return $pdf->download('assets-list.pdf');
    }

    public function export()
    {
        return Excel::download(new AssetsExport, 'Assets.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function addimport()
    {
        return view('admin.assets.imports');
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
    $valid_extension = array("csv", "xlsx");
    $maxFileSize = 2097152;
    if (in_array(strtolower($extension), $valid_extension)) {
    if ($fileSize <= $maxFileSize) {
    } else {
    throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    } else {
    throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
    }
    }

    public function import()
    {
        $file = request()->file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $this->checkUploadedFileProperties($extension, $fileSize);
        $location = 'uploads';
        $file->move($location, $filename);
        $filepath = public_path($location . "/" . $filename);

        if($extension == "csv"){
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);
            if ($i == 0) {
            $i++;
            continue;
            }
            for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
            }
            $i++;
            }
            fclose($file);
            $j = 0;
            foreach ($importData_arr as $importData) {
                // dd($importData[0]);
                $catego = Category::where('code',$importData[0])->get();
                if($catego->count() == 0){
                    return redirect()->route('assets.addimport')->withSuccess('Category Code Not Exits!');
                }
                $scatego = SubCategory::where('code',$importData[1])->get();
                // dd($scatego->count());
                if($scatego->count() == 0){
                    return redirect()->route('assets.addimport')->withSuccess('Sub Category Code Not Exits!');
                }
                foreach ($catego as $key => $categos) {
                   $catn = $categos->id;
                   $catc = $categos->code;
                }
                foreach ($scatego as $key => $scategos) {
                   $scatn = $scategos->id;
                   $scatc = $scategos->code;
                }
                // dd($catn);
                $j++;
                try {
                    $icno = DB::table('reference_no')->where('id',1)->get();
                        foreach ($icno as $key => $icnos) {
                            $icno1 = $icnos->item_code;
                        }
                DB::beginTransaction();
                    Asset::create([
                        'category_id' => $catn,
                        'subcategory_id' => $scatn,
                        'code' => $catc.'-'.$scatc.'-'.$icno1,
                        'name' => $importData[2],
                        'unit_id' => $importData[3],
                        'price' => $importData[4],
                        'purchase_date' => $importData[5],
                        'is_damage' => 'no',
                        'assigned' => 0,
                        'status' => 1
                    ]);
                    $icno2 = $icno1 + 1;
                    DB::update('update reference_no set item_code = '.$icno2.' where id = ?', ['1']);
                    DB::commit();

                }catch (\Exception $e) {
                    DB::rollBack();
                    }
            }

                return redirect()->route('assets.addimport')->withSuccess('Assets Import Successful!');
            }else{
                return redirect()->route('assets.addimport')->withSuccess('Wrong File extention!');
        }
    }

}
