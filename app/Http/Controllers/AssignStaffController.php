<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\Asset;
use App\Models\Staff;
use App\Models\Category;
use App\Models\AssignStaff;
use DB;

class AssignStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = AssignStaff::query();
        $query =    $query  ->leftJoin('rooms', 'rooms.id', '=', 'assignstaffs.room_id')
                            ->leftJoin('floors', 'floors.id', '=', 'assignstaffs.floor_id')
                            ->leftJoin('assets', 'assets.id', '=', 'assignstaffs.asset_id');
        $query = $query->select('assignstaffs.*', 'assets.name as assetname','floors.name as floorname','rooms.name as roomname');
        if (request('term')) {
            $term = request('term');
            $query->where('assets.name', 'Like', '%' . $term . '%');
        }
        $assignstaff = $query->orderBy('id', 'DESC')->paginate(15);
        return view('admin.assignstaffs.index', compact('assignstaff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assets = Asset::where('status', 1)->where('assigned',0)->get();
        $rooms = DB::table('rooms')->get();
        $floors = DB::table('floors')->get();
        return view('admin.assignstaffs.create', compact('assets','rooms','floors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // store category
        $assignstaff =
        DB::table('assignstaffs')->insert([
            'room_id' => $request->room_id,
            'floor_id' => $request->floor_id,
            'asset_id' => $request->assetName,
            'date' => $request->date,
        ]);
        DB::table('assets')->where('id',$request->assetName)->update([
            'assigned' => '1',
        ]);
        return redirect()->route('assets.index')->withSuccess('Assign Asset successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignstaff = AssignStaff::where('id', $id)->first();
        $assignstaff->delete();
        return redirect()->route('assignstaffs.index')->withSuccess('Assign Staff deleted successfully!');
    }
}
