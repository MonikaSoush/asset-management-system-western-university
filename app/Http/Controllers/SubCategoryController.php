<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\GeneralSetting;
use App\Models\Size;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use PDF;
use DB;
use App\Exports\SubCategoryExport;
use Maatwebsite\Excel\Facades\Excel;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the sub categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SubCategory::query();
        if (request('term')) {
            $term = request('term');
            $query = SubCategory::where('name', 'Like', '%' . $term . '%')
                ->orWhere('note', 'Like', '%' . $term . '%')
                ->orWhereHas('category', function ($newQuery) use ($term) {
                    $newQuery->where('name', 'LIKE', '%' . $term . '%');
                });
        }
        $subCategories = $query->orderBy('id', 'DESC')->paginate(15);
        return view('admin.sub-categories.index', compact('subCategories'));
    }

    /**
     * Show the form for creating a new sub category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 1)->get();
        $sizes = Size::where('status', 1)->get();
        return view('admin.sub-categories.create', compact('categories', 'sizes'));
    }

    /**
     * Store a newly created sub category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form
        $validator = $request->validate([
            'name' => 'required|string|max:60|unique:sub_categories',
            'categoryName' => 'required|integer',
            'note' => 'nullable|string|max:255',
        ]);
        $sccode = GeneralSetting::where('key', 'subcategoryCodePrefix')->firstOrFail()->value;

        // store subcategory
        $subCategory = SubCategory::create([
            'name' => $request->name,
            'category_id' => $request->categoryName,
            'code' => $sccode.$request->code,
            'sizes' => '1',
            'note' => clean($request->note),
            'status' => $request->status
        ]);
        return redirect()->route('subCategories.index')->withSuccess('Sub category created successfully!');
    }

    /**
     * Display the specified sub category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return redirect()->route('subCategories.index');
    }

    /**
     * Show the form for editing the specified sub category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $categories = Category::where('status', 1)->get();
        $subCategory = SubCategory::where('slug', $slug)->first();
        return view('admin.sub-categories.edit', compact('categories', 'subCategory'));
    }

    /**
     * Update the specified sub category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $subCategory = SubCategory::where('slug', $slug)->first();

        // validate form
        $validator = $request->validate([
            'name' => 'required|string|max:60|unique:sub_categories,name,' . $subCategory->id,
            'categoryName' => 'required|integer',
            'note' => 'nullable|string|max:255',
        ]);

        // update subcategory
        $subCategory->update([
            'name' => $request->name,
            'code' => $request->code,
            'category_id' => $request->categoryName,
            'sizes' => '1',
            'note' => clean($request->note),
            'status' => $request->status
        ]);
        return redirect()->route('subCategories.index')->withSuccess('Sub category updated successfully!');
    }

    /**
     * Remove the specified sub category from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $subCategory = SubCategory::where('slug', $slug)->first();

        // destroy sub category
        $subCategory->delete();
        return redirect()->route('subCategories.index')->withSuccess('Sub category deleted successfully!');
    }

    /**
     * Change the status of specified sub category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($slug)
    {
        $subCategory = SubCategory::where('slug', $slug)->first();

        // change sub category status
        if ($subCategory->status == 1) {
            $subCategory->update([
                'status' => 0
            ]);
        } else {
            $subCategory->update([
                'status' => 1
            ]);
        }
        return redirect()->route('subCategories.index')->withSuccess('Sub category status changed successfully!');
    }

    // create pdf
    public function createPDF()
    {
        // retreive all records from db
        $data = SubCategory::with('category')->latest()->get();
        // share data to view
        view()->share('subCategories', $data);
        $pdf = PDF::loadView('admin.pdf.sub-categories', $data->all());
        // download PDF file with download method
        return $pdf->download('sub-categories-list.pdf');
    }

    public function imports(){
        return view('admin.subcategoeyimport.imports');
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
    $valid_extension = array("csv", "xlsx");
    $maxFileSize = 2097152;
    if (in_array(strtolower($extension), $valid_extension)) {
    if ($fileSize <= $maxFileSize) {
    } else {
    throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    } else {
    throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
    }
    }

    public function import()
    {
        $file = request()->file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $this->checkUploadedFileProperties($extension, $fileSize);
        $location = 'uploads';
        $file->move($location, $filename);
        $filepath = public_path($location . "/" . $filename);

        if($extension == "csv"){
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);
            if ($i == 0) {
            $i++;
            continue;
            }
            for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
            }
            $i++;
            }
            fclose($file);
            $j = 0;
            foreach ($importData_arr as $importData) {
                $cat =  Category::where('code', $importData[0])->get();
                foreach ($cat as $key => $cats) {
                    $cat_id = $cats->id;
                }
                if($cat->count() == 0){
                    return redirect()->route('subcategoeyimport.imports')->withSuccess('Category Does Exits!');
                }
                $j++;
                try {

                DB::beginTransaction();
                    SubCategory::create([
                        'category_id' => $cat_id,
                        'code' => $importData[1],
                        'name' => $importData[2],
                        'slug' => $importData[2],
                        'note' => $importData[3],
                        'status' => 1
                    ]);
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollBack();
                    }
            }
                return redirect()->route('subcategoeyimport.imports')->withSuccess('Sub Category Improts Successful!');
            }else{
                return redirect()->route('subcategoeyimport.imports')->withSuccess('Wrong File extention!');
        }
    }

    public function export()
    {
        return Excel::download(new SubCategoryExport, 'Subcategory.xlsx');
    }
}
