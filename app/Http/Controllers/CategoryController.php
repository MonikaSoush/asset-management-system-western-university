<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use PDF;
use DB;
use App\Exports\CategoriesExport;
use Maatwebsite\Excel\Facades\Excel;

class CategoryController extends Controller
{
    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Category::query();
        if (request('term')) {
            $term = request('term');
            $query->where('name', 'Like', '%' . $term . '%');
        }
        $categories = $query->orderBy('id', 'DESC')->paginate(15);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories',
            'note' => 'nullable|string|max:255',
        ]);
        $ccode = GeneralSetting::where('key', 'categoryCodePrefix')->firstOrFail()->value;
        // store category
        $category = Category::create([
            'name' => $request->name,
            'code' => $ccode.$request->code,
            'note' => clean($request->note),
            'status' => $request->status
        ]);
        return redirect()->route('categories.index')->withSuccess('Category added successfully!');
    }

    /**
     * Display the specified category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $category = Category::where('slug', $slug)->first();
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $category = Category::where('slug', $slug)->first();

        //validate form
        $validator = $request->validate([
            'name' => 'required|string|max:50|unique:categories,name,' . $category->id,
            'note' => 'nullable|string|max:255',
        ]);

        // update category
        $category->update([
            'name' => $request->name,
            'code' => $request->code,
            'note' => clean($request->note),
            'status' => $request->status
        ]);
        return redirect()->route('categories.index')->withSuccess('Category updated successfully!');
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $category = Category::where('slug', $slug)->first();
        // destroy category
        $category->delete();
        return redirect()->route('categories.index')->withSuccess('Category deleted successfully!');
    }


    /**
     * Change the status of specified category.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($slug)
    {
        $category = Category::where('slug', $slug)->first();

        // change category status
        if ($category->status == 1) {
            $category->update([
                'status' => 0
            ]);
        } else {
            $category->update([
                'status' => 1
            ]);
        }
        return redirect()->route('categories.index')->withSuccess('Category status changed successfully!');
    }

    // create pdf
    public function createPDF()
    {
        // retreive all records from db
        $data = Category::latest()->get();
        // share data to view
        view()->share('categories', $data);
        $pdf = PDF::loadView('admin.pdf.categories', $data->all());
        // download PDF file with download method
        return $pdf->download('categories-list.pdf');
    }

    public function addimports()
    {
        return view('admin.categories.imports');
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
    $valid_extension = array("csv", "xlsx");
    $maxFileSize = 2097152;
    if (in_array(strtolower($extension), $valid_extension)) {
    if ($fileSize <= $maxFileSize) {
    } else {
    throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    } else {
    throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
    }
    }

    public function import()
    {
        $file = request()->file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $this->checkUploadedFileProperties($extension, $fileSize);
        $location = 'uploads';
        $file->move($location, $filename);
        $filepath = public_path($location . "/" . $filename);

        if($extension == "csv"){
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);
            if ($i == 0) {
            $i++;
            continue;
            }
            for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
            }
            $i++;
            }
            fclose($file);
            $j = 0;
            foreach ($importData_arr as $importData) {
                $j++;
                try {
                DB::beginTransaction();
                    Category::create([
                        'code' => $importData[0],
                        'name' => $importData[1],
                        'note' => $importData[2],
                        'status' => 1
                    ]);
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollBack();
                    }
            }

                return redirect()->route('categories.addimports')->withSuccess('User Import Successful!');
            }else{
                return redirect()->route('categories.addimports')->withSuccess('Wrong File extention!');
        }
    }

    public function export()
    {
        return Excel::download(new CategoriesExport, 'Category.xlsx');
    }
}
