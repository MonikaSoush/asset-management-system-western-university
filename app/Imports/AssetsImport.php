<?php

namespace App\Imports;

use App\Models\Asset;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Hash;
use DB;

class AssetsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Asset([
            'name' => $row['name'],
            'code' => $row['code'],
            'price' => $row['price'],
            'subcategory_id' => $row['subcategory_id'],
            'category_id' => $row['category_id'],
            'purchase_date' => $row['purchase_date'],
            'is_damage' => 'no',
            'assigned' => 1,
            'status' => 1
        ]);

    }
}
