<?php

namespace App\Exports;

use App\Models\SubCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SubCategoryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SubCategory::select("sub_categories.id",'categories.name as cname','sub_categories.code', 'sub_categories.name as subname', 'sub_categories.slug', 'sub_categories.note', 'sub_categories.status')->join('categories','categories.id','=','sub_categories.category_id','LEFT')->get();
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings(): array
    {
        return ["ID", "Category Name","Code", 'Name', 'Slug', 'Note', 'Status'];
    }
}
