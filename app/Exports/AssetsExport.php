<?php

namespace App\Exports;

use App\Models\Asset;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Asset::select("assets.id",'assets.code as acode', 'assets.name as aname', 'assets.price', 'assets.status as astt', 'categories.name as cname', 'sub_categories.name as subname','assets.unit_id','assets.assigned','assets.is_damage','assets.purchase_date','assets.depreciation')->join('categories','categories.id','=','assets.category_id','left')->join('sub_categories','sub_categories.id','=','assets.subcategory_id','left')->get();
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings(): array
    {
        return ["ID", "Code", 'Name', 'Price', 'Status', 'Category_id', 'Subcategory_id','Unit_id','Assigned','Is Damage','Purchase Date','Depreciation'];
    }
}
